<?php

namespace Ensi\PackingClient;

class PackingClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\PackingClient\Api\ExamplesApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\PackingClient\Dto\Error',
        '\Ensi\PackingClient\Dto\PaginationTypeEnum',
        '\Ensi\PackingClient\Dto\EmptyDataResponse',
        '\Ensi\PackingClient\Dto\ErrorResponse',
        '\Ensi\PackingClient\Dto\ModelInterface',
    ];

    /** @var string */
    public static $configuration = '\Ensi\PackingClient\Configuration';
}
